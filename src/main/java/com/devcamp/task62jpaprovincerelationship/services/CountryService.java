package com.devcamp.task62jpaprovincerelationship.services;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task62jpaprovincerelationship.model.Region;
import com.devcamp.task62jpaprovincerelationship.model.Country;
import com.devcamp.task62jpaprovincerelationship.repository.ICountryRepository;

@Service
public class CountryService {
    @Autowired
    ICountryRepository countryRepository;
    public ArrayList<Country> getAllCountries(){
        ArrayList<Country> countryList = new ArrayList<>();
        countryRepository.findAll().forEach(countryList:: add);
        return countryList;
    }
    public Set<Region> getRegionsByCountryId(int id){
        Country vCountry = countryRepository.findById(id);
        if ( vCountry != null){
            return vCountry.getRegions();
        }
        else return null;
    }

}

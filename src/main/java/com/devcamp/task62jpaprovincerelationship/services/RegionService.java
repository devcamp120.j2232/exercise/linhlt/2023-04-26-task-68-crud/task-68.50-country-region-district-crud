package com.devcamp.task62jpaprovincerelationship.services;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task62jpaprovincerelationship.model.Region;
import com.devcamp.task62jpaprovincerelationship.model.District;
import com.devcamp.task62jpaprovincerelationship.repository.IRegionRepository;

@Service
public class RegionService {
    @Autowired
    IRegionRepository regionRepository;
    public ArrayList<Region> getAllRegions(){
        ArrayList<Region> regionList = new ArrayList<>();
        regionRepository.findAll().forEach(regionList:: add);
        return regionList;
    }
    public Set<District> getDistrictsByRegionId(int id){
        Region vRegion = regionRepository.findById(id);
        if ( vRegion != null){
            return vRegion.getDistricts();
        }
        else return null;
    }

}

package com.devcamp.task62jpaprovincerelationship.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task62jpaprovincerelationship.model.District;
import com.devcamp.task62jpaprovincerelationship.repository.IDistrictRepository;

@Service
public class DistrictService {
    @Autowired
    IDistrictRepository districtRepository;
    public ArrayList<District> getAllDistricts(){
        ArrayList<District> districtList = new ArrayList<>();
        districtRepository.findAll().forEach(districtList:: add);
        return districtList;
    }
}

package com.devcamp.task62jpaprovincerelationship.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62jpaprovincerelationship.model.Region;
import com.devcamp.task62jpaprovincerelationship.model.District;
import com.devcamp.task62jpaprovincerelationship.repository.IRegionRepository;
import com.devcamp.task62jpaprovincerelationship.repository.IDistrictRepository;
import com.devcamp.task62jpaprovincerelationship.services.RegionService;
import com.devcamp.task62jpaprovincerelationship.services.DistrictService;

@RestController
@RequestMapping("/district")
@CrossOrigin
public class DistrictController {
    @Autowired
    RegionService regionService;
    @Autowired
    DistrictService districtService;
    
    //get all districts lisst
    @GetMapping("/all")
    public ResponseEntity<List<District>> getAlldistricts(){
        try {
            return new ResponseEntity<>(districtService.getAllDistricts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //get districts by region id
    @GetMapping("/search")
    public ResponseEntity<Set<District>> getdistrictsByRegionIdApi(@RequestParam(value = "regionId") int id){
        try {
            Set<District> regionDistricts = regionService.getDistrictsByRegionId(id);
            if (regionDistricts != null ){
                return new ResponseEntity<>(regionDistricts, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //get district detail by id
    @Autowired
    IDistrictRepository districtRepository;
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getDistrictById(@PathVariable(name="id") int id){
        District district = districtRepository.findById(id);
        if (district != null){
            return new ResponseEntity<>(district, HttpStatus.OK);
        } else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    //create new district with region id
    @Autowired
    IRegionRepository regionRepository;
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createDistrict(@PathVariable("id") int id, @RequestBody District pDistrict){
        try {
            Region region = regionRepository.findById(id);
            if (region !=null){
                District newDistrict = new District();
                newDistrict.setName(pDistrict.getName());
                newDistrict.setPrefix(pDistrict.getPrefix());
                newDistrict.setRegion(region);
                District _district = districtRepository.save(newDistrict);
                //district _district = districtRepository.save(pDistrict);
                return new ResponseEntity<>(_district, HttpStatus.CREATED);
            } else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //update district
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updatedistrict(@PathVariable(name="id")int id, @RequestBody District pDistrictUpdate){
        try {
           District district = districtRepository.findById(id);
            if (district != null){
                district.setName(pDistrictUpdate.getName());
                district.setPrefix(pDistrictUpdate.getPrefix());
                district.setRegion(pDistrictUpdate.getRegion());
                
                return new ResponseEntity<>(districtRepository.save(district), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     //delete district by Id
     @DeleteMapping("/delete/{id}")
     public ResponseEntity<District> deletedistrictById(@PathVariable("id") int id) {
         try {
            District district = districtRepository.findById(id);
            districtRepository.delete(district);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
}

package com.devcamp.task62jpaprovincerelationship.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62jpaprovincerelationship.model.Region;
import com.devcamp.task62jpaprovincerelationship.model.Country;
import com.devcamp.task62jpaprovincerelationship.repository.IRegionRepository;
import com.devcamp.task62jpaprovincerelationship.repository.ICountryRepository;
import com.devcamp.task62jpaprovincerelationship.services.RegionService;
import com.devcamp.task62jpaprovincerelationship.services.CountryService;

@RestController
@CrossOrigin
@RequestMapping("/region")
public class RegionController {
    @Autowired
    CountryService countryService;
    @Autowired
    RegionService regionService;
    //get all regions lisst
    @GetMapping("/all")
    public ResponseEntity<List<Region>> getAllregions(){
        try {
            return new ResponseEntity<>(regionService.getAllRegions(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //get region by province id
    @GetMapping("/search")
    public ResponseEntity<Set<Region>> getRegionsByCountryIdApi(@RequestParam(value = "countryId") int id){
        try {
            Set<Region> countryRegions = countryService.getRegionsByCountryId(id);
            if (countryRegions != null ){
                return new ResponseEntity<>(countryRegions, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //get region detail by id
    @Autowired
    IRegionRepository regionRepository;
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getregionById(@PathVariable int id){
        Region region = regionRepository.findById(id);
        if (region != null){
            return new ResponseEntity<>(region, HttpStatus.OK);
        } else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    //create new region with country id
    @Autowired
    ICountryRepository countryRepository;
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createregion(@PathVariable("id") int id, @RequestBody Region pregion){
        try {
            Country country = countryRepository.findById(id);
            if (country != null){
                Region newRegion = new Region();
                newRegion.setName(pregion.getName());
                newRegion.setCode(pregion.getCode());
                newRegion.setCountry(country);
                Region _region = regionRepository.save(newRegion);
                //region _region = regionRepository.save(pregion);
                return new ResponseEntity<>(_region, HttpStatus.CREATED);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //update region
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateregion(@PathVariable(name="id")int id, @RequestBody Region pregionUpdate){
        try {
           Region region = regionRepository.findById(id);
            if (region != null){
                region.setName(pregionUpdate.getName());
                region.setCode(pregionUpdate.getCode());
                region.setCountry(pregionUpdate.getCountry());
                
                return new ResponseEntity<>(regionRepository.save(region), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     //delete region by Id
     @DeleteMapping("/delete/{id}")
     public ResponseEntity<Region> deleteRegionById(@PathVariable("id") int id) {
        try {
            Region region = regionRepository.findById(id);
            regionRepository.delete(region);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
     }
}

package com.devcamp.task62jpaprovincerelationship.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62jpaprovincerelationship.model.Country;
import com.devcamp.task62jpaprovincerelationship.repository.ICountryRepository;
import com.devcamp.task62jpaprovincerelationship.services.CountryService;

@RestController
@CrossOrigin
@RequestMapping("/country")
public class CountryController {
    @Autowired
    CountryService countryService;
    
    //get all countries list
    @GetMapping("/all")
    public ResponseEntity<List<Country>> getAllCountries(){
        try {
            return new ResponseEntity<>(countryService.getAllCountries(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //get country detail by id
    @Autowired
    ICountryRepository countryRepository;
    
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getcountryById(@PathVariable int id){
        Country country = countryRepository.findById(id);
        if (country != null){
            return new ResponseEntity<>(country, HttpStatus.OK);
        } else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //create new country
    @PostMapping("/create")
    public ResponseEntity<Object> createCountry(@RequestBody Country pCountry){
        try {
            Country newCountry = new Country();
            newCountry.setCode(pCountry.getCode());
            newCountry.setName(pCountry.getName());
            
            return new ResponseEntity<>(countryRepository.save(newCountry), HttpStatus.CREATED);

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //update country
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updatecountry(@PathVariable(name="id")int id, @RequestBody Country pCountryUpdate){
        try {
           Country country = countryRepository.findById(id);
            if (country != null){
                country.setCode(pCountryUpdate.getCode());
                country.setName(pCountryUpdate.getName());
                
                return new ResponseEntity<>(countryRepository.save(country), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     //delete country by Id
     @DeleteMapping("/delete/{id}")
     public ResponseEntity<Country> deletecountryById(@PathVariable("id") int id) {
         try {
            Country country = countryRepository.findById(id); 
            countryRepository.delete(country);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
 

}

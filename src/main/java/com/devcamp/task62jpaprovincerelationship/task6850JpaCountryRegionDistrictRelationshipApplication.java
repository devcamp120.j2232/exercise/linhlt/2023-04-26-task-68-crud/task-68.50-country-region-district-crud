package com.devcamp.task62jpaprovincerelationship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class task6850JpaCountryRegionDistrictRelationshipApplication {

	public static void main(String[] args) {
		SpringApplication.run(task6850JpaCountryRegionDistrictRelationshipApplication.class, args);
	}

}

package com.devcamp.task62jpaprovincerelationship.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table (name = "region")
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Region {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column (name = "name")
    private String name;

    @Column (name = "code", unique = true)
    private String code;

    @ManyToOne
    @JsonIgnore
    private Country country;

    @OneToMany (targetEntity = District.class, cascade = CascadeType.ALL)
    @JoinColumn (name = "region_id")
    private Set<District> districts;

    public Region() {
    }
    public Region(int id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public Region(int id, String name, String code, Country country, Set<District> districts) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.country = country;
        this.districts = districts;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public Country getCountry() {
        return country;
    }
    public void setCountry(Country country) {
        this.country = country;
    }
    public Set<District> getDistricts() {
        return districts;
    }
    public void setDistricts(Set<District> districts) {
        this.districts = districts;
    }

}

package com.devcamp.task62jpaprovincerelationship.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table (name = "district")
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column (name="name")
    private String name;

    @Column (name = "prefix")
    private String prefix;

    @ManyToOne
    @JsonIgnore
    private Region region;
    
    public District() {
    }
    public District(int id, String name, String prefix) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
    }
    
    public District(int id, String name, String prefix, Region region) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.region = region;
    }
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPrefix() {
        return prefix;
    }
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    public Region getRegion() {
        return region;
    }
    public void setRegion(Region region) {
        this.region = region;
    }
    
}

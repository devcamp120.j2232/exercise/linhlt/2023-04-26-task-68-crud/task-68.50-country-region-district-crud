package com.devcamp.task62jpaprovincerelationship.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62jpaprovincerelationship.model.Region;
import com.devcamp.task62jpaprovincerelationship.model.Country;

public interface IRegionRepository extends JpaRepository<Region, Long>{
    Region findById(int id);
    List<Region> findByCountryId(int id);
    Optional<Country> findByIdAndCountryId(Long id, Long instructorId);
}

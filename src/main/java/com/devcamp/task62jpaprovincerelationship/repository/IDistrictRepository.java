package com.devcamp.task62jpaprovincerelationship.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62jpaprovincerelationship.model.Region;
import com.devcamp.task62jpaprovincerelationship.model.District;

public interface IDistrictRepository extends JpaRepository<District, Long> {
    District findById(int id);
    List<Region> findByRegionId(int id);
    Optional<Region> findByIdAndRegionId(Long id, Long instructorId);
}

package com.devcamp.task62jpaprovincerelationship.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62jpaprovincerelationship.model.Country;

public interface ICountryRepository extends JpaRepository<Country, Long>{
    Country findById(int id);
    
}
